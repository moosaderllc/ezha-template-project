#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *
import random

from ezha import BaseState
from ezha import FontManager
from ezha import LanguageManager
from ezha import ConfigManager
from ezha import MenuManager
from ezha import SoundManager
from ezha import AnimatedObject
from ezha import AnimatedVelocityObject
from ezha import Label
from ezha import PercentBar
from ezha import Utilities

class DemoShmupState( BaseState ):
    def __init__( self ):
        self.stateName = "demoshmupstate"
        self.images = {
            "background" : pygame.image.load( "assets/graphics/backgrounds/background.png" ),
            "shmup_art" : pygame.image.load( "assets/graphics/characters/shmup_art.png" )
        }

        self.ship = AnimatedVelocityObject( { "image" : self.images["shmup_art"],
            "posRect" : pygame.Rect( 100, 720/2-16, 32, 32 ),
            "frameRect" : pygame.Rect( 0, 0, 32, 32 ),
            "maxRows" : 4, "maxCols" : 2,
            "animateSpeed" : 0.1, "row" : 0
            } )
            
        self.enemies = []
        self.bullets = []
        self.shootTimer = 0
        self.shootCooldownLength = 20
        self.score = 0
        self.scoreLabel = Label( {
            "label_text" : str( self.score ),
            "label_position" : ( 1200, 10 ),
            "label_font" : FontManager.Get( "main" ),
            "label_color" : pygame.Color( 50, 50, 50 ),
            "label_secondary_color" : pygame.Color( 255, 255, 255 ),
            "label_style" : "outline"
        } )
        self.instructions = [
            Label( {
                "label_text" : "ARROW KEYS: Movement",
                "label_position" : ( 200, 175 ),
                "label_font" : FontManager.Get( "main" ),
                "label_color" : pygame.Color( 50, 50, 50 ),
                "label_secondary_color" : pygame.Color( 255, 255, 255 ),
                "label_style" : "outline" } ),

            Label( {
                "label_text" : "SPACE BAR: Shoot",
                "label_position" : ( 200, 250 ),
                "label_font" : FontManager.Get( "main" ),
                "label_color" : pygame.Color( 50, 50, 50 ),
                "label_secondary_color" : pygame.Color( 255, 255, 255 ),
                "label_style" : "outline" } ),

            Label( {
                "label_text" : "Shoot the aliens!",
                "label_position" : ( 200, 325 ),
                "label_font" : FontManager.Get( "main" ),
                "label_color" : pygame.Color( 50, 50, 50 ),
                "label_secondary_color" : pygame.Color( 255, 255, 255 ),
                "label_style" : "outline" } ),

            Label( {
                "label_text" : "Press ENTER to begin",
                "label_position" : ( 200, 485 ),
                "label_font" : FontManager.Get( "main" ),
                "label_color" : pygame.Color( 50, 50, 50 ),
                "label_secondary_color" : pygame.Color( 255, 255, 255 ),
                "label_style" : "outline" } ),

            Label( {
                "label_text" : "Or ESC to go back to menu",
                "label_position" : ( 200, 560 ),
                "label_font" : FontManager.Get( "main" ),
                "label_color" : pygame.Color( 50, 50, 50 ),
                "label_secondary_color" : pygame.Color( 255, 255, 255 ),
                "label_style" : "outline" } ) 
        ]
        
        self.life = 100
        self.hurtTimer = 0
        self.invincibilityLength = 50
        self.lifeBar = PercentBar( {
            "bar_color" : pygame.Color( 0, 255, 0 ),
            "width" : 200, "height" : 20,
            "initial_value" : self.life,
            "position" : pygame.Rect( 10, 10, 0, 0 )
            } )

        SoundManager.Add( { "key" : "music", "path" : "assets/audio/Spaced_Moosader.ogg", "isMusic" : True } )
        SoundManager.Add( { "key" : "explosion", "path" : "assets/audio/explosion.wav", "isMusic" : False } )
        SoundManager.Add( { "key" : "hurt", "path" : "assets/audio/hurt.wav", "isMusic" : False } )
            
        self.fpsLabel = Label( {
            "label_font" : FontManager.Get( "main" ),
            "label_text" : "FPS: " + str( int( Utilities.GetFPSClock().get_fps() ) ),
            "label_position" : ( 10, 650 ),
            "label_color" : pygame.Color( 255, 255, 255 ),
            "label_secondary_color" : pygame.Color( 0, 0, 0 ),
            "label_style" : "outline"
            } )
        
        self.paused = True

        
    def Setup( self, screenWidth, screenHeight ):
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight
        self.gotoState = ""

        self.SpawnEnemy()

        SoundManager.PlayMusic( "music" )


    def SpawnEnemy( self ):
        randomY = random.randint( 0, 720 )
        velocityX = random.uniform( -4, -6 )
        velocityY = random.uniform( -2, 2 )
        whichAlien = 1
        
        if ( velocityY <= -0.5 ):
            whichAlien = 2
        elif ( velocityY >= 0.5 ):
            whichAlien = 3
        else:
            velocityY = 0
        
        enemy = AnimatedVelocityObject()
        enemy.Setup( { "image" : self.images["shmup_art"],
            "posRect" : pygame.Rect( 1280, randomY, 32, 32 ),
            "frameRect" : pygame.Rect( 0, 0, 32, 32 ),
            "maxRows" : 4, "maxCols" : 2,
            "animateSpeed" : 0.1, "row" : whichAlien,
            "xVelocity" : velocityX, "yVelocity" : velocityY,
            "accelerationRate" : 0, "deaccelerationRate" : 0
            } )

        self.enemies.append( enemy )
        
        
    def Update( self ):
        # Check for quit or back to menu
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
                
            if ( event.type == KEYDOWN ):
                if ( event.key == K_ESCAPE ):
                    self.gotoState = "titlestate"

                if ( event.key == K_RETURN ):
                    self.paused = False
                
        if ( not self.paused ):
            self.ship.Update()

            # Player input
            keys = pygame.key.get_pressed()
            shipPos = self.ship.GetPosition()
            
            if ( keys[ K_UP ] ):
                shipPos.y -= 5
                
            elif ( keys[ K_DOWN ] ):
                shipPos.y += 5
                
            if ( keys[ K_LEFT ] ):
                shipPos.x -= 5
                
            elif ( keys[ K_RIGHT ] ):
                shipPos.x += 5

            if ( keys[ K_SPACE ] and self.shootTimer <= 0 ):
                self.AddBullet()
                self.shootTimer = self.shootCooldownLength

            self.ship.SetPosition( { "x" : shipPos.x, "y" : shipPos.y } )

            # Random enemy creation
            makeEnemy = random.randint( 0, 10 )
            if ( makeEnemy == 0 ):
                self.SpawnEnemy()

            deleteEnemy = []
            for key in range( len( self.enemies ) ):
                self.enemies[key].Update()
                
                # Delete enemies once they're offscreen
                if ( self.enemies[key].GetPosition().x < -32 ):
                    deleteEnemy.append( key )

            # Update bullets
            deleteBullets = []
            for key in range( len( self.bullets ) ):
                self.bullets[key].Update()

                # Delete bullets once they're offscreen
                if ( self.bullets[key].GetPosition().x > 1280 ):
                    deleteBullets.append( key )

            # Collisions
            for ek in range( len( self.enemies ) ):
                if ( self.hurtTimer <= 0 and Utilities.RectCollision( self.ship.GetPosition(), self.enemies[ek].GetPosition() ) ):
                    self.hurtTimer = self.invincibilityLength
                    self.life -= 10
                    self.lifeBar.ChangePercentage( self.life )
                    SoundManager.PlaySound( "hurt" )

                for bk in range( len( self.bullets ) ):
                    if ( Utilities.RectCollision( self.bullets[bk].GetPosition(), self.enemies[ek].GetPosition() ) ):
                        SoundManager.PlaySound( "explosion" )
                        if ( bk not in deleteBullets ):
                            deleteBullets.append( bk )
                        if ( ek not in deleteEnemy ):
                            deleteEnemy.append( ek )
                        self.score += 10
                        self.scoreLabel.ChangeText( str( self.score ) )
                        
            
            for bk in range( len( deleteBullets )-1, -1, -1 ):
                self.bullets.pop( deleteBullets[bk] )
                    
            for ek in range( len( deleteEnemy )-1, -1, -1 ):
                self.enemies.pop( deleteEnemy[ek] )

            # Invincibility Timer
            if ( self.hurtTimer > 0 ):
                self.hurtTimer -= 1

            # Shoot Timer
            if ( self.shootTimer > 0 ):
                self.shootTimer -= 1

            # Game over state
            if ( self.life <= 0 ):
                self.paused = True
                self.instructions = [
                    Label( {
                    "label_text" : "Game Over",
                    "label_position" : ( 1280/2-100, 720/2 ),
                    "label_font" : FontManager.Get( "main" ),
                    "label_color" : pygame.Color( 255, 255, 255 ),
                    "label_secondary_color" : pygame.Color( 0, 0, 0 ),
                    "label_style" : "outline" } )
                ]

            # FPS label
            self.fpsLabel.ChangeText( "FPS: " + str( int( Utilities.GetFPSClock().get_fps() ) ) )


        
    
    def Draw( self, windowSurface ):
        windowSurface.blit( self.images["background"], ( 0, 0 ) )

        if ( self.paused ):
            for instruction in self.instructions:
                instruction.Draw( windowSurface )
        else:
            self.ship.Draw( { "window" : windowSurface } )

            for enemy in self.enemies:
                enemy.Draw( { "window" : windowSurface } )

            for bullet in self.bullets:
                bullet.Draw( { "window" : windowSurface } )

            self.lifeBar.Draw( windowSurface )

            self.scoreLabel.Draw( windowSurface )

            self.fpsLabel.Draw( windowSurface )

    
    def Clear( self ):
        print( "HI" )

    def AddBullet( self ):
        SoundManager.PlaySound( "cancel" )
        position = self.ship.GetPosition()
        self.bullets.append( AnimatedVelocityObject( { "image" : self.images["shmup_art"],
            "posRect" : position,
            "frameRect" : pygame.Rect( 0, 0, 32, 32 ),
            "maxRows" : 4, "maxCols" : 2,
            "animateSpeed" : 0.1, "row" : 4,
            "xVelocity" : 1, "yVelocity" : 0,
            "maxAcceleration" : 5, "maxVelocity" : 10,
            "accelerationRate" : 1, "deaccelerationRate" : 0,
            "autoAccelerate" : [0.1,0]
            } ) )
