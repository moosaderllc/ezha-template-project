#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from ezha import BaseState
from ezha import FontManager
from ezha import LanguageManager
from ezha import ConfigManager
from ezha import MenuManager
from ezha import Character
from ezha import AnimatedObject
from ezha import Utilities
from ezha import Label
from ezha import Map
from ezha import Camera

class DemoArpgState( BaseState ):
    def __init__( self ):
        self.stateName = "gamestate"
        self.images = {}
        self.player = None
        self.fpsLabel = None
        self.levelMap = None
        self.camera = None


    def Setup( self, screenWidth, screenHeight ):
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight
        self.gotoState = ""

        self.images["background"] = pygame.image.load( "assets/graphics/ui/background_title.png" )
        self.images["player"] = pygame.image.load( "assets/graphics/characters/girl.png" )
        self.images["tileset"] = pygame.image.load( "assets/graphics/tilesets/tileset.png" )
        
        self.player = AnimatedObject()
        self.player.Setup( { "image" : self.images["player"],
            "posRect" : pygame.Rect( 1280/2, 720/2, 32, 48 ),
            "frameRect" : pygame.Rect( 0, 0, 32, 48 ),
            "collisionRect" : pygame.Rect( 0, 32, 32, 12 ),
            "maxRows" : 4, "maxCols" : 4,
            "animateSpeed" : 0.1, "row" : 0
            } )
            
        #self.player.SetInputs( {    # Defaults to the arrow keys
            #"up" : K_w,
            #"down" : K_s,
            #"left" : K_a,
            #"right" : K_d,
            #"run" : K_LSHIFT
            #}, True     # Allow diagonal movement
            #)
            
        #self.player.SetAnimationMappings( { "idle-south" : 0, "idle-north" : 1, "idle-west" : 2, "idle-east" : 3,
                                            #"walk-south" : 4, "walk-north" : 5, "walk-west" : 6, "walk-east" : 7,
                                            #"run-south"  : 8, "run-north"  : 9, "run-west" : 10, "run-east" : 11
                                            #} )
        #self.player.currentAction = "idle-south"
        
        self.fpsLabel = Label()
        self.fpsLabel.Setup( {
            "label_font" : FontManager.Get( "main" ),
            "label_text" : "FPS: " + str( int( Utilities.GetFPSClock().get_fps() ) ),
            "label_position" : ( 10, 10 ),
            "label_color" : pygame.Color( 255, 255, 255 ),
            "label_secondary_color" : pygame.Color( 0, 0, 0 ),
            "label_style" : "outline"
            } )

        self.levelMap = Map()
        self.levelMap.LoadJsonMap( "assets/tiledmaps/map.json", self.images["tileset"] )

        self.camera = Camera()
        self.camera.Setup( screenWidth, screenHeight )
        #self.camera.SetRange( 0, 0, 320, 240 )
        self.camera.SetRange( 0, 0, self.levelMap.GetPixelWidth() * 0.25, self.levelMap.GetPixelHeight() * 0.25 )
        # map is 1600 x 960


    def Update( self ):
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()

            if ( event.type == KEYDOWN ):
                if ( event.key == K_ESCAPE ):
                    self.gotoState = "titlestate"

            if event.type == MOUSEBUTTONUP:
                mousex, mousey = event.pos

        keys = pygame.key.get_pressed()

        # temp
        if ( keys[ K_z ] ):
            print( "Offset:", self.camera.GetOffsetX(), self.camera.GetOffsetY(), "Player:", self.player.GetRect().x, self.player.GetRect().y  )
            
        #movedX, movedY = self.player.HandleInput( keys, self.levelMap )
        self.player.Update()
        self.camera.CenterOn( self.player.GetPosition() )

        self.fpsLabel.ChangeText( "FPS: " + str( int( Utilities.GetFPSClock().get_fps() ) ) )


    def Draw( self, windowSurface ):
        # windowSurface.blit( self.images["background"], ( 0, 0 ) )
        self.levelMap.DrawBelow( windowSurface, self.camera )
        self.player.Draw( { "window" : windowSurface } ) #, self.camera )
        self.levelMap.DrawAbove( windowSurface, self.camera )
        self.fpsLabel.Draw( windowSurface )


    def Clear( self ):
        self.levelMap.Clear()
        print( "Clear" )


