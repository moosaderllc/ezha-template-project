#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from ezha import BaseState
from ezha import FontManager
from ezha import LanguageManager
from ezha import ConfigManager
from ezha import MenuManager
from ezha import SoundManager
from ezha import Label

class OptionsState( BaseState ):
    def __init__( self ):
        self.stateName = "optionsstate"
        
    def Setup( self, screenWidth, screenHeight ):
        self.screenWidth = screenWidth
        self.screenHeight = screenHeight
        self.gotoState = ""

        MenuManager.Load( "assets/menus/optionsscreen.menu" )
        MenuManager.labelItems[ "soundVolumeValue" ].ChangeText( str( int( SoundManager.GetSoundVolume() * 100 ) ) + "%" )
        MenuManager.labelItems[ "musicVolumeValue" ].ChangeText( str( int( SoundManager.GetMusicVolume() * 100 ) ) + "%" )
        
        
    def Update( self ):        
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            
            if event.type == MOUSEBUTTONUP:
                mouseX, mouseY = event.pos    

                clickAction = MenuManager.ButtonAction( mouseX, mouseY )
                
                if ( clickAction == "music_down" ):
                    SoundManager.SetMusicVolume( SoundManager.GetMusicVolume() - 0.10 )
                    
                elif ( clickAction == "music_up" ):
                    SoundManager.SetMusicVolume( SoundManager.GetMusicVolume() + 0.10 )
                    
                elif ( clickAction == "sound_down" ):
                    SoundManager.SetSoundVolume( SoundManager.GetSoundVolume() - 0.10 )
                    
                elif ( clickAction == "sound_up" ):
                    SoundManager.SetSoundVolume( SoundManager.GetSoundVolume() + 0.10 )
                    
                elif ( clickAction != "" ):
                    ConfigManager.Save()
                    self.gotoState = clickAction

                self.UpdateLabels()

    def UpdateLabels( self ):
        MenuManager.labelItems[ "soundVolumeValue" ].ChangeText( str( int( SoundManager.GetSoundVolume() * 100 ) ) + "%" )
        MenuManager.labelItems[ "musicVolumeValue" ].ChangeText( str( int( SoundManager.GetMusicVolume() * 100 ) ) + "%" )
    
    def Draw( self, windowSurface ):
        MenuManager.Draw( windowSurface )
        

    def Clear( self ):
        MenuManager.Clear()
        
