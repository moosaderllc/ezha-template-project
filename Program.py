#!/usr/bin/python
# -*- coding: utf-8 -*-

import pygame, sys
from pygame.locals import *

from states import TitleState
from states import HelpState
from states import OptionsState
from states import DemoSelectState
from states import DemoArpgState
from states import DemoPlatformerState
from states import DemoShmupState

from ezha import ConfigManager
from ezha import FontManager
from ezha import LanguageManager
from ezha import SoundManager
from ezha import MenuManager
from ezha import Utilities
from ezha import Logger

class Program( object ):
    
    def __init__( self ):
        self.fpsClock = None
        self.windowSurface = None
        self.currentState = None
        self.screenWidth = 1280
        self.screenHeight = 720
        self.isDone = False
        self.fps = 60
        self.clearColor = pygame.Color( 151, 255, 140 )
        self.states = {}
    
    def Init( self ):
        Logger.Init( True )
        pygame.init()
        self.fpsClock = pygame.time.Clock()
        Utilities.SetFPSClock( self.fpsClock )
        self.windowSurface = pygame.display.set_mode( ( self.screenWidth, self.screenHeight ) )
        
        ConfigManager.Load()
        
        language = ConfigManager.options[ "language" ].lower()
        
        SoundManager.Add( { "key" : "confirm", "path" : "assets/audio/ui-confirm.wav", "isMusic" : False } )
        SoundManager.Add( { "key" : "cancel", "path" : "assets/audio/ui-cancel.wav", "isMusic" : False } )
        SoundManager.SetMusicVolume( float( ConfigManager.options["music volume"] ) / 100 )
        SoundManager.SetSoundVolume( float( ConfigManager.options["sound volume"] ) / 100 )
        MenuManager.SetButtonAudio( "confirm", "cancel" )
        
        LanguageManager.Setup( { "helperLanguage" : language, "targetLanguage" : language } )
        
        pygame.display.set_caption( "TEMPLATE GAME" )
        FontManager.Add( "main", "assets/fonts/Averia-Regular.ttf", 40 )
        FontManager.Add( "main_big", "assets/fonts/Averia-Bold.ttf", 70 )

        self.states[ "titlestate" ] = TitleState()
        self.states[ "optionsstate" ] = OptionsState()
        self.states[ "helpstate" ] = HelpState()
        self.states[ "demoselectstate" ] = DemoSelectState()
        self.states[ "demoarpgstate" ] = DemoArpgState()
        self.states[ "demoshmupstate" ] = DemoShmupState()
        self.states[ "demoplatformerstate" ] = DemoPlatformerState()

        self.SwitchState( "titlestate" )
        
    def Run( self ):
        while ( self.isDone is False ):
            self.Update()
            self.Draw()

            gotoState = self.currentState.GotoState()
            if ( gotoState != "" ):
                self.SwitchState( gotoState )
    
    def Update( self ):
        self.currentState.Update()
        
    def Draw( self ):
        self.windowSurface.fill( self.clearColor )
        
        self.currentState.Draw( self.windowSurface )
        
        pygame.display.update()
        self.fpsClock.tick( self.fps )

    def SwitchState( self, name ):
        if ( name == "exit" ):
            print( "Goodbye!" )
            pygame.quit()
            sys.exit()
        
        if ( self.currentState != None ):
            self.currentState.Clear()
            
        self.currentState = self.states[ name ]
        self.currentState.Setup( self.screenWidth, self.screenHeight )
